#!/usr/bin/python
# -*- coding: utf-8 -*-

import json, datetime, requests, boto3, io
from smart_open import open as sopen

from src.shared_functions import _compress_utf8_file, _get_env, _get_logger

logger = _get_logger()

s3_bucket = _get_env('s3_bucket')
s3_key_process_out = _get_env('s3_key_process_out')
const_s3_process_out = 's3://' + s3_bucket + '/' + s3_key_process_out

s3_client = boto3.client('s3')

def add_increments_last_hour(utcs, measures, last_hour, last_hour_now):
    def get_rest(rest):
        # Add variable-fake increments
        if rest > 55:
            return 1
        elif rest > 50:
            return 2
        elif rest > 45:
            return 1
        elif rest > 40:
            return 2
        elif rest > 35:
            return 1
        elif rest > 30:
            return 3
        elif rest > 25:
            return 1
        elif rest > 20:
            return 2
        elif rest > 15:
            return 1
        elif rest > 10:
            return 2
        else:
            return 1

    # Add increments per minute accessing the last hour.
    out = {}
    out_prev = {}
    min_now = datetime.datetime.now().minute

    for i,v in enumerate(utcs):
        cat = v[2] # i.e A1, A1-B1, ...

        pageviews = 0

        try:
            # Note: measures[i] is like the x in the previous sum in the grand_total but pointing to the right category
            pageviews = measures[i][0][last_hour]
        except:
            continue

        if not pageviews:
            continue

        try:
            increment_per_minute = int(pageviews / 60)
            rest = pageviews % 60
        except:
            increment_per_minute = 0
            rest = 0

        if last_hour == last_hour_now:
            for i in range(60):
                if i < min_now:
                    add = get_rest(rest) if rest > 0 else 0
                    rest -= add

                    if cat not in out_prev:
                        out_prev[cat] = 0
                    out_prev[cat] += increment_per_minute + add

                elif i == min_now:
                    add = get_rest(rest) if rest > 0 else 0
                    rest -= add

                    if cat not in out:
                        out[cat] = 0
                    out[cat] += increment_per_minute + add
        else:
            if cat not in out_prev:
                out_prev[cat] = 0
            out_prev[cat] += pageviews

    return out, out_prev
### End ### def add_increments_last_hour ###


def calculate_top_categories(data_w, data_pw, total_weekly):
    parsed = {}
    total_weekly = 1 if total_weekly == 0 else int(total_weekly)

    for k in data_w:
        a = (int(data_w[k]) + 1.0001) / total_weekly
        minus = 0 if k not in data_pw else int(data_pw[k])
        b = (int(data_w[k]) - minus + 1.0001) / total_weekly
        parsed[k] = a * b

    parsed = sorted(parsed.items(), key=lambda kv: kv[1], reverse=True)[0:5]

    return [x for x in parsed]
### End ### def calculate_top_categories ###


def get_wiki_excerpt(page, lang):
    try:
        wiki_res = requests.get("https://"+lang+".wikipedia.org/api/rest_v1/page/summary/" + page)

        """ Example wiki_res
        {
            'type': 'standard',
            'title': 'Sensorization',
            'displaytitle': 'Sensorization',
            'namespace': {'id': 0, 'text': ''},
            'titles': {'canonical': 'Sensorization', 'normalized': 'Sensorization', 'display': 'Sensorization'},
            'pageid': 41716792,
            'lang': 'en',
            'dir': 'ltr',
            'revision': '778556617',
            'tid': '6d572188-a406-11e8-8b0d-048d352f6d1b',
            'timestamp': '2017-05-03T21:24:14Z',
            'content_urls': {
                'desktop': {
                    'page': 'https://en.wikipedia.org/wiki/Sensorization',
                    'revisions': 'https://en.wikipedia.org/wiki/Sensorization?action=history',
                    'edit': 'https://en.wikipedia.org/wiki/Sensorization?action=edit',
                    'talk': 'https://en.wikipedia.org/wiki/Talk:Sensorization'
                },
                'mobile': {...}
            },
            'api_urls': {...},
            'extract': 'Sensorization is a modern technology trend to insert many similar sensors in any device or application. ...',
            'extract_html': '<p><b>Sensorization</b> is a modern technology trend to insert many similar sensors in any device or application....</p>'
        }
        """
        wiki_json_res = wiki_res.json()

        return wiki_json_res['extract']
    except Exception as e:
        logger.info("[get_wiki_excerpt] --- ERROR", e)
        return 'no excerpt was found...'
### End ### def get_wiki_excerpt ###

def get_topics(topics_wc, topics_wp, total_wc, all_categories_info, lang):
    topics = []
    top_categories = calculate_top_categories(topics_wc, topics_wp, total_wc)

    for p in top_categories:
        if p[0] in all_categories_info:
            cat_info = all_categories_info[p[0]]

            topics.append({
                'category': p[0],
                'value': "{:.10f}".format(float(p[1])),
                'total': topics_wc[p[0]],
                'url': cat_info['url'],
                'title': cat_info['title'],
                'text': get_wiki_excerpt(cat_info['page'], lang)
            })

    return topics
### End ### def get_topics ###


def handler(event=None, context=None):

    try: # SNS Message
        mess = event['Records'][0]['Sns']['Message'].strip("\n")
        message = json.loads(mess)
    except: # CRON Message
        message = event

    # these are used into the SQS message of the next process.
    channel_name = message['channel_name'] # the name of the output channel
    channel_lang = message['channel_lang'] # the language of the output channel

    s3_path = const_s3_process_out +'/channel='+channel_name+'/lang='+channel_lang+'/'

    # Input files
    file_json_daily = s3_path + 'wikipedia_compact_daily.json'
    file_json_hourly = s3_path + 'wikipedia_compact_hourly.json'
    file_json_hourly_yesterday = s3_path + 'wikipedia_compact_hourly_yesterday.json' ## TODO - Use this
    last_level_cat = 4

    grand_total = 0 # sum of all pageviews for all categories
    grand_total_increments = 0 # sum of all pageviews increments per run
    total_wc = 0 # sum of the pageviews in the current week for all categories
    total_wp = 0 # sum of the pageviews in the previous week for all categories
    totals_wc = {} # sum of the pageviews in the current week for each category
    totals_wp = {} # sum of the pageviews in the previous week for each category
    hot_topics_wc = {} # same of totals_wc but only for the `last_level_cat` level (i.e. aX-bY-cW-dZ)
    hot_topics_wp = {} # same of totals_wp but only for the `last_level_cat` level (i.e. aX-bY-cW-dZ)
    trending_contents_wc = {} # same of totals_wc but for all categories in the 1st level (aX, aX-*, ...)
    trending_contents_wp = {} # same of totals_wp but for all categories in the 1st level (aX, aX-*, ...)
    all_categories_info = {} # collect all info for each category

    tmp_filepath_out = '/tmp/wikipedia_compact_daily.json.gz'
    with sopen(file_json_daily, mode='rb') as wcd:
        with io.open(tmp_filepath_out, 'wb') as f:
            f.write(wcd.read())

    # First collect yearly data
    with sopen(tmp_filepath_out, mode='r', encoding='utf-8') as wcd:
        for a in wcd:

            b = json.loads(a)

            utcs = b['url_title_category']
            lang = b['language']
            #dates = b['date']
            measures = b['measure']

            grand_total = sum([sum(x) for x in measures])

            for i,v in enumerate(utcs):
                cat = v[2] # i.e A1, A1-B1, ...
                cat_parts = cat.split('-')
                parent_cat = cat_parts[0]
                cat_len = len(cat_parts)

                if cat not in all_categories_info:
                    all_categories_info[cat] = {
                        'url': v[0],
                        'title': v[1],
                        'category': v[2],
                        'page': v[0].split('/')[-1]
                    }

                try:
                    # sum all values of the last week
                    totals_wc[cat] = sum(measures[i][-7:])
                    total_wc += totals_wc[cat]
                except:
                    pass

                try:
                    # sum all values of the previous week
                    totals_wp[cat] = sum(measures[i][-14:-7])
                    total_wp += totals_wp[cat]
                except:
                    pass

                # > HOT TOPICS
                if cat_len == last_level_cat: # get only last level categories
                    hot_topics_wc[cat] = totals_wc[cat]
                    hot_topics_wp[cat] = totals_wp[cat]

                # > Trending Topics current week: sum all values for all main categories and all their descendants
                if parent_cat not in trending_contents_wc:
                    trending_contents_wc[parent_cat] = 0

                trending_contents_wc[parent_cat] += totals_wc[cat]

                # > Trending Topics previous week: sum all values for all main categories and all their descendants
                if parent_cat not in trending_contents_wp:
                    trending_contents_wp[parent_cat] = 0

                trending_contents_wp[parent_cat] += totals_wp[cat]


    logger.info("-- Daily grand total: " + str(grand_total))

    # Then collect last day hourly data up to the last retrieved hour (excluded).
    # The last retrieved hour will be used to create increments per minute.

    tmp_filepath_out = '/tmp/wikipedia_compact_hourly.json.gz'
    with sopen(file_json_hourly, mode='rb') as wcd:
        with io.open(tmp_filepath_out, 'wb') as f:
            f.write(wcd.read())

    # First collect yearly data
    with sopen(tmp_filepath_out, mode='r', encoding='utf-8') as wcd:
        for a in wcd:
            b = json.loads(a)
            utcs = b['url_title_category']
            lang = b['language']
            dates = b['date']
            measures = b['measure']

            try:
                last_hour = int(b['last_hour'])
                last_hour_now = datetime.datetime.now().hour - 3 # Lambda is executed on UTC0
                logger.info("-- Retrieved last available hour: [" + str(last_hour) + "] -- Calculated current hour: [" + str(last_hour_now) + "] Date: " + str(dates[0]))
            except:
                last_hour = datetime.datetime.now().hour - 3 # Lambda is executed on UTC0

            if last_hour < 0:
                break

            increments, prev_increments = add_increments_last_hour(utcs, measures, last_hour, last_hour_now)

            if last_hour > 0: # if 0 just add increments below

                # Note: x is a something like [[6, 2, 0, 0, 2, 1, 4, 15, 17, 29, 23, ... ]] so, x[0] will take the inner list.
                grand_total += sum([sum(x[0][:last_hour]) for x in measures])

                # add all increments in the previous minutes to the total counter
                grand_total += sum(prev_increments.values())

                for i,v in enumerate(utcs):
                    cat = v[2] # i.e A1, A1-B1, ...
                    cat_parts = cat.split('-')
                    parent_cat = cat_parts[0]
                    cat_len = len(cat_parts)

                    if cat not in all_categories_info:
                        all_categories_info[cat] = {
                            'url': v[0],
                            'title': v[1],
                            'category': v[2],
                            'page': v[0].split('/')[-1]
                        }

                    if cat in prev_increments:
                        addon = prev_increments[cat]
                    else:
                        addon = 0

                    if cat in increments:
                        # Add minute increments to the grand total
                        grand_total_increments += increments[cat]

                    try:
                        # Note: measures[i] is like the x in the previous sum in the grand_total but pointing to the right category
                        totals_wc[cat] = sum(measures[i][0][:last_hour])
                        totals_wc[cat] += addon

                        total_wc += totals_wc[cat]
                        total_wc += addon
                    except:
                        pass

                    # > HOT TOPICS
                    if cat_len == last_level_cat: # get only last level categories

                        if cat not in hot_topics_wc:
                            hot_topics_wc[cat] = totals_wc[cat]
                        else:
                            hot_topics_wc[cat] += totals_wc[cat]

                    # > Trending Topics current week: sum all values for all main categories and all their descendants
                    if parent_cat not in trending_contents_wc:
                        trending_contents_wc[parent_cat] = 0

                    trending_contents_wc[parent_cat] += totals_wc[cat]


    logger.info("-- Last day grand total: " + str(grand_total) + ' + increments: ' + str(grand_total_increments))

    topics = {
        'hot_topics': get_topics(hot_topics_wc, hot_topics_wp, total_wc, all_categories_info, lang),
        'trending_contents': get_topics(trending_contents_wc, trending_contents_wp, total_wc, all_categories_info, lang),
        'total': str(grand_total),
        'total_incremented': str(grand_total_increments)
    }

    filename_json = 'wikipedia_compact_totals.json'

    tmp_filepath_out = '/tmp/' + filename_json
    with io.open(tmp_filepath_out, 'w', encoding='utf-8') as f:
        f.write(json.dumps(topics, ensure_ascii=False))

    _compress_utf8_file(tmp_filepath_out)

    remote_file_path = s3_key_process_out+'/channel='+channel_name+'/lang='+channel_lang+'/'+filename_json
    s3_client.put_object(
        Bucket=s3_bucket,
        Key=remote_file_path,
        ContentType='application/json',
        ContentEncoding='gzip',
        Body=open(tmp_filepath_out+".gz", 'rb')
    )

    logger.info("########## File of totals has been saved.")