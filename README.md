# Wikipedia Data Consumer

This repository contains the code used to read Wikipedia Data from the Data Lake, owned by Linkalab, and to produce a JSON used in a custom Dashboard (made with Angular 4).
You need some manual work on the Athena side and you need to use the [serverless framework](https://www.serverless.com/) to deploy the ETL.

## Requirement

1. You need to get access to the Data Lake `prod.datalake.linkalab` (DLLK for the sake of brevity).

2. You may need to [create an Athena Workgroup](https://docs.aws.amazon.com/it_it/athena/latest/ug/workgroups-create-update-delete.html#creating-workgroups) with the option "**Enable queries on Requester Pays buckets in Amazon S3**" enabled or you can enable this option on the default workgroup (primary).
NOTE: without this you will get an error
```diff
- com.amazonaws.services.s3.model.AmazonS3Exception: Access Denied
```
when doing a query on Athena or:
```diff
- An error occurred (InvalidRequestException) when calling the StartQueryExecution operation: Unable to verify/create output bucket prod.datalake.linkalab
```
when running Lambdas.

If a new Workgroup has been added, take a note of its name and edit the serverless accordingly (change the value of `athena_workgroup_name`).



## Athena Setup

### Database

Create a database on Athena:

```sql
CREATE database wikipedia_databasename;
```

### Create table *daily_csv*

Create a table to handle daily CSV data. The first three columns are, respectively, for the domain, the page title and daily pageviews, [as defined here](https://dumps.wikimedia.org/other/pagecounts-ez/). The remaining columns represent pageviews for each hour (0-23) which originally was represented with a compact string (A6C3 is equivalent to 6 pageviews at midnight and 3 pageviews at 2AM).

Here is the code:

```sql
CREATE EXTERNAL TABLE `wikipedia_databasename.daily_csv`(
  `project` string,
  `page` string,
  `tot` INT,
  `h00` INT, `h01` INT, `h02` INT, `h03` INT, `h04` INT, `h05` INT,
  `h06` INT, `h07` INT, `h08` INT, `h09` INT, `h10` INT, `h11` INT,
  `h12` INT, `h13` INT, `h14` INT, `h15` INT, `h16` INT, `h17` INT,
  `h18` INT, `h19` INT, `h20` INT, `h21` INT, `h22` INT, `h23` INT
)
PARTITIONED BY (
  `date_p` INT
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY ' '
  ESCAPED BY '\\'
  LINES TERMINATED BY '\n'
LOCATION
  's3://prod.datalake.linkalab/management/cleaned/wikipedia/daily/'
```

***NOTE***: Location points to DLLK and data is partitioned by dumpdate (date_p).

#### Load partitions

Load all historical data:

```sql
MSCK REPAIR TABLE wikipedia_databasename.daily_csv;
```

***NOTE***: New partitions will be loaded every day with an "ADD PARTITION" within a lambda.


### Create a table for the Editorial Plan

Create a table for editorial plans, replacing `{{a-bucket-inside-your-account}}` in the Location with a bucket name in your account:

```sql
CREATE EXTERNAL TABLE IF NOT EXISTS `wikipedia_databasename.editorial_plans` (
  `category` string,
  `title` string,
  `project` string,
  `page` string
)
PARTITIONED BY (
  `channel` string,
  `lang` string
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
WITH SERDEPROPERTIES (
  'serialization.format' = '\;',
  'field.delim' = '\073'
) LOCATION 's3://{{a-bucket-inside-your-account}}/wikipedia-editorial-plans/'
TBLPROPERTIES ('has_encrypted_data'='false');
```

You can also change the subpath `wikipedia-editorial-plans` if needed.
Inside this subpath you need to create the partitioning channel=xx/lang=yy

i.e.:

wikipedia-editorial-plans/channel=mychannel/lang=it/file.csv

The file.csv must be like this:

```csv
A1;Città metropolitana di Cagliari;it.z;Città_metropolitana_di_Cagliari
A1-B1;Cagliari;it.z;Cagliari
...and so on
```

on which columns are:

- Category: i.e. A1, A1-B1, A1-B2, ..., A1-B1-C1, ..., and so on
- Title: a title used for visualizations (can be the same name of the page)
- Wiki project language: i.e. "it.z" (the .z must be present because is used to join this table with original data)
- Page: the page part of a Wikipedia Link. I.e. "Città_metropolitana_di_Cagliari" from the link https://it.wikipedia.org/wiki/Citt%C3%A0_metropolitana_di_Cagliari (do not encode accent marks)


Then load partitions:

```sql
MSCK REPAIR TABLE wikipedia_databasename.editorial_plans;
```

### Create table *hourly_csv*

We also need to create a table for the hourly data which is [originally defined in the Wikipedia dump](https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Traffic/Pageviews#Contained_data):

```sql
CREATE EXTERNAL TABLE `wikipedia_databasename.hourly_csv`(
  `project` string,
  `page` string,
  `pageview` INT,
  `unused` string
)
PARTITIONED BY (
  `date_p` INT,
  `hour_p` INT
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY ' '
  ESCAPED BY '\\'
  LINES TERMINATED BY '\n'
LOCATION
  's3://prod.datalake.linkalab/intake/raw/wikipedia/hourly/'
```

Then load partitions:

```sql
MSCK REPAIR TABLE wikipedia_databasename.hourly_csv;
```

Note that inside this database the main project is without the ".z" part.

**We need to get the same structure as the daily version, so we need to add a view**

```sql
CREATE OR REPLACE VIEW wikipedia_databasename.view_merged_hours AS

  WITH page_one_row_per_hour AS (
    SELECT h.project, h.page, date_p,
      NULLIF (( cast((hour_p =  0 ) AS INTEGER) * pageview), 0) AS h00, NULLIF (( cast((hour_p =  1 ) AS INTEGER) * pageview), 0) AS h01,
      NULLIF (( cast((hour_p =  2 ) AS INTEGER) * pageview), 0) AS h02, NULLIF (( cast((hour_p =  3 ) AS INTEGER) * pageview), 0) AS h03,
      NULLIF (( cast((hour_p =  4 ) AS INTEGER) * pageview), 0) AS h04, NULLIF (( cast((hour_p =  5 ) AS INTEGER) * pageview), 0) AS h05,
      NULLIF (( cast((hour_p =  6 ) AS INTEGER) * pageview), 0) AS h06, NULLIF (( cast((hour_p =  7 ) AS INTEGER) * pageview), 0) AS h07,
      NULLIF (( cast((hour_p =  8 ) AS INTEGER) * pageview), 0) AS h08, NULLIF (( cast((hour_p =  9 ) AS INTEGER) * pageview), 0) AS h09,
      NULLIF (( cast((hour_p = 10 ) AS INTEGER) * pageview), 0) AS h10, NULLIF (( cast((hour_p = 11 ) AS INTEGER) * pageview), 0) AS h11,
      NULLIF (( cast((hour_p = 12 ) AS INTEGER) * pageview), 0) AS h12, NULLIF (( cast((hour_p = 13 ) AS INTEGER) * pageview), 0) AS h13,
      NULLIF (( cast((hour_p = 14 ) AS INTEGER) * pageview), 0) AS h14, NULLIF (( cast((hour_p = 15 ) AS INTEGER) * pageview), 0) AS h15,
      NULLIF (( cast((hour_p = 16 ) AS INTEGER) * pageview), 0) AS h16, NULLIF (( cast((hour_p = 17 ) AS INTEGER) * pageview), 0) AS h17,
      NULLIF (( cast((hour_p = 18 ) AS INTEGER) * pageview), 0) AS h18, NULLIF (( cast((hour_p = 19 ) AS INTEGER) * pageview), 0) AS h19,
      NULLIF (( cast((hour_p = 20 ) AS INTEGER) * pageview), 0) AS h20, NULLIF (( cast((hour_p = 21 ) AS INTEGER) * pageview), 0) AS h21,
      NULLIF (( cast((hour_p = 22 ) AS INTEGER) * pageview), 0) AS h22, NULLIF (( cast((hour_p = 23 ) AS INTEGER) * pageview), 0) AS h23
    FROM "wikipedia_databasename"."hourly_csv" AS h
        RIGHT OUTER JOIN "wikipedia_databasename"."editorial_plans" AS ep
            ON h.page = ep.page
    WHERE date_p >= cast(date_format((now()- interval '1' day), '%Y%m%d') AS integer)
  )

  SELECT project, page, date_p,
    SUM(h00) AS h00, SUM(h01) AS h01, SUM(h02) AS h02, SUM(h03) AS h03, SUM(h04) AS h04, SUM(h05) AS h05,
    SUM(h06) AS h06, SUM(h07) AS h07, SUM(h08) AS h08, SUM(h09) AS h09, SUM(h10) AS h10, SUM(h11) AS h11,
    SUM(h12) AS h12, SUM(h13) AS h13, SUM(h14) AS h14, SUM(h15) AS h15, SUM(h16) AS h16, SUM(h17) AS h17,
    SUM(h18) AS h18, SUM(h19) AS h19, SUM(h20) AS h20, SUM(h21) AS h21, SUM(h22) AS h22, SUM(h23) AS h23
  FROM page_one_row_per_hour
  GROUP BY project, page, date_p
```

***NOTE*** The view performs a huge query and you can incur in additional costs if not limited with editorial_plans table and date_p.


## Deploy

You firs need [serverless framework](https://www.serverless.com/), then you must change the `custom` variables in the `serverless.yml`.
You can deploy with:

```
~$ sls deploy
```


### Lambda do_big_query

Scheduled to run each day with a cron event.

Can be invoked via SNS:

```json
{
    "projects": "it.z",
    "channel_name": "mychannel",
    "channel_lang": "it",
    "wiki_base_url": "https://it.wikipedia.org/wiki/",
    "query_type": "daily" //// "hourly"
}
```

This will run a query in background, sending a SQS message (see `queue_name_athena_create` in the `serverless.yml`) like this:

```json
{
    "query_execution_id": "2a31133d-1d23-45fe-b7c5-3826d90be764",
    "channel_name": "mychannel",
    "channel_lang": "it",
    "wiki_base_url": "https://it.wikipedia.org/wiki/",
    "query_type": "daily" ////"hourly"
}
```

### Lambda parse_big_query_result

This start with a SQS message which is read every 30 minutes and will create a JSON file `wikipedia_compact_....json` used inside custom visualization dashboard.



### Lambda make totals

This lambda is invoked every minute and for each page `K` in the editorial plan applies the formula:

```
[ ( Ʃ pwKwc + 1.0001 ) / Ʃ pwAwc ] * [ ( Ʃ pwKwc - Ʃ pwKwp + 1.0001 ) / Ʃ pwAwc ]
```

where:

- Ʃ pwKwc = sum of pageviews in the current week of page K
- Ʃ pwKwp = sum of pageviews in the previous week of page
- Ʃ pwAwc = sum of pageviews of the current week for all the pages

Results are stored to S3 in a JSON file (i.e. wikipedia_compact_totals.json)

Can be invoked also with SNS:

```
{
    "channel_name": "mychannel",
    "channel_lang": "it"
}
```


# Authors

"Wikipedia Data Consumer" is created by [Linkalab S.r.l.](http://www.linkalab.it)


# License

GNU General Public License v3.0 or later

See `LICENSE` to see the full text.