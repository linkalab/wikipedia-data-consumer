#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys, time, re, json, boto3, datetime, csv, io
from smart_open import open as sopen
from time import gmtime, strftime
from datetime import timedelta, date, datetime
from botocore.exceptions import ClientError

from src.shared_functions import _compress_utf8_file, _get_env, _get_logger, _natural_key

logger = _get_logger()

# Environment variables
wiki_project = _get_env('wiki_project')

s3_bucket = _get_env('s3_bucket')
s3_key_output_athena = _get_env('s3_key_output_athena')
s3_key_process_out = _get_env('s3_key_process_out')

const_athena_s3_output = 's3://' + s3_bucket + '/' + s3_key_output_athena + '/'
const_athena_db = _get_env('athena_db')
athena_workgroup_name = os.getenv('athena_workgroup_name', 'primary')  # Default to primary

project_name = _get_env('project_name')

# SES
ses_sender = _get_env("ses_sender")
ses_recipient = _get_env("ses_recipient")

# Boto3 clients/resources
sqs_resource = boto3.resource('sqs')
s3_client = boto3.client('s3')
athena_client = boto3.client('athena')
ses_client = boto3.client('ses')

queue_name_athena_create = _get_env('queue_name_athena_create')


def parse_big_query_result(event=None, context=None):
    """
    Create JSON from athena query
    """

    i = 0
    max_int = sys.maxsize
    while i < 100:
        i = i + 1
        # decrease the max_int value by factor 10 as long as the OverflowError occurs.
        try:
            csv.field_size_limit(max_int)
            break
        except OverflowError:
            max_int = int(max_int/10)

    try:
        body = json.loads(event['Records'][0]['body'])
        logger.info('Reading SQS: %s' % body)

        """
        SQS body contains:
        {
            "query_execution_id": "the id of the athena execution query for the `query_type` version",
            "query_type": "daily" (default) or "hourly"
            "channel_name": "the name of the output channel",
            "channel_lang": "the language of the output channel"
            "wiki_base_url": "the wikipedia base url to be concatenated with the page value"
        }
        """

        channel_name = body['channel_name']
        channel_lang = body['channel_lang']
        wiki_base_url = body['wiki_base_url']
        query_type = body['query_type'] if 'query_type' in body else 'daily'

        query_execution_id = body['query_execution_id']
        query_execution = athena_client.get_query_execution(QueryExecutionId=query_execution_id)

        # https://docs.aws.amazon.com/athena/latest/APIReference/API_QueryExecutionStatus.html
        query_execution_status = query_execution['QueryExecution']['Status']['State']

        if query_execution_status == 'FAILED' or query_execution_status == 'CANCELLED':
            send_mail("Query execution for id %s ended with status %s" % (query_execution_id, query_execution_status))

        elif query_execution_status == 'SUCCEEDED':
            # Query is endend with success. Next step is to read the query output

            # Get the bytes from S3
            s3_object_name = s3_key_output_athena + '/' + query_execution_id + '.csv'

            tmp_filepath_in = '/tmp/athena-' + query_execution_id + '.csv'

            s3_client.download_file(s3_bucket, s3_object_name, tmp_filepath_in) # Download this file to writable tmp space.

            out_utc = {}
            out_tots = {}
            out_dates = []

            ## CSV out
            csv_columns = [ [''],[''],[''] ]

            last_hour = -1 # initialize to -1

            got_dates = False

            # Read CSV rows to create an ordered list
            with sopen(tmp_filepath_in, mode='r') as opr:
                csvreader = csv.DictReader(opr)

                # row is something like:
                # OrderedDict([
                #   ('category', 'A1-B2-C4'),
                #   ('page', 'Cixerri'),
                #   ('title', 'Cixerri'),
                #   ('tot_array', '[[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]'),
                #   ('date_array', '[20200115]'),
                #   ('max_h_array', '[14]')
                # ])

                for row in csvreader:

                    url = wiki_base_url + row['page']
                    title = row['title']
                    category = row['category']

                    if query_type == 'daily':
                        ##-- CSV out --##
                        csv_columns[0].append(title)
                        csv_columns[1].append(category)
                        csv_columns[2].append(url)

                    if got_dates == False:
                        # We need dates array just once.
                        out_dates_int = json.loads( row['date_array'] )

                        if query_type == 'daily':
                            ##-- CSV out --##
                            for ia,the_date in enumerate(out_dates_int):
                                # add new row with the date inside as first element of the list
                                csv_columns.append([the_date])

                        got_dates = True

                    out_utc[category] = [ url, title, category ]
                    out_tots[category] = json.loads( row['tot_array'] )

                    if query_type == 'daily':
                        ##-- CSV out --##
                        for ib,the_pageviews in enumerate(out_tots[category]):
                            # add the pageviews in a columnar way
                            csv_columns[ib+3].append(the_pageviews)

                    if query_type != 'daily':
                        try:
                            last_hour = json.loads( row['max_h_array'] )[0]
                        except Exception as e:
                            logger.error(e)
                            pass

            # sort using natural keys (A1, A1-B1, ..., A1-B9, A1-B10, ..., B1, ...)
            sorted_keys = sorted(out_utc, key=_natural_key)

            # use sorted keys to reorder both utc and tots arrays.
            sorted_out_utc = {}
            sorted_out_tots = {}
            for key in sorted_keys:
                sorted_out_utc[key] = out_utc[key]
                sorted_out_tots[key] = out_tots[key]

            # transform 20190128 into "2019-01-28"
            for d in out_dates_int:
                out_dates.append( str(d)[0:4]+'-'+str(d)[4:6]+'-'+str(d)[6:8] )

            # Set the structure of the JSON
            output_json = {
                "url_title_category": [ k for k in sorted_out_utc.values() ],
                "language": channel_lang,
                "date": out_dates,
                "measure": [ k for k in sorted_out_tots.values() ],
                "ch_name": channel_name
            }

            logger.info("Query type: " + query_type)
            logger.info("Last hour: " + str(last_hour))

            if query_type != 'daily' and last_hour > -1:
                output_json['last_hour'] = last_hour

            filename_json = 'wikipedia_compact_' + query_type + '.json'

            tmp_filepath_out = '/tmp/' + filename_json
            with io.open(tmp_filepath_out, 'w', encoding='utf-8') as f:
                f.write(json.dumps(output_json, ensure_ascii=False))

            _compress_utf8_file(tmp_filepath_out)

            remote_file_path = s3_key_process_out+'/channel='+channel_name+'/lang='+channel_lang+'/'+filename_json
            s3_client.put_object(
                Bucket=s3_bucket,
                Key=remote_file_path,
                ContentType='application/json',
                ContentEncoding='gzip',
                Body=open(tmp_filepath_out+".gz", 'rb')
            )

            if query_type == 'daily':

                ### STORE ALSO THE CSV VERSION
                filename_csv = 'wikipedia_pageviews.csv'
                tmp_filepath_out_csv = '/tmp/'+channel_name+'_'+channel_lang+'_'+filename_csv
                with io.open(tmp_filepath_out_csv, 'w', encoding='utf-8') as f:
                    wr = csv.writer(f, quoting=csv.QUOTE_ALL, dialect='excel')
                    wr.writerows(csv_columns)

                remote_file_path = s3_key_process_out+'/channel='+channel_name+'/lang='+channel_lang+'/'+filename_csv
                s3_client.put_object(
                    Bucket=s3_bucket,
                    Key=remote_file_path,
                    Body=open(tmp_filepath_out_csv, 'rb')
                )

                ### make a backup using the day of the week ONLY for daily version
                now = datetime.now()
                backup_time = datetime.strftime(now, '%a')
                remote_file_path_bkp = s3_key_process_out+'/channel='+channel_name+'/lang='+channel_lang+'/backup/'+backup_time+'_'+filename_json
                s3_client.put_object(
                    Bucket=s3_bucket,
                    Key=remote_file_path_bkp,
                    ContentType='application/json',
                    ContentEncoding='gzip',
                    Body=open(tmp_filepath_out+".gz", 'rb')
                )

        else: # QUEUED | RUNNING
            raise Exception("Query status is QUEUED or RUNNING. Add the message again to the queue.")

    except Exception as e:
        logger.error(e)
        raise e


def send_mail(body_text, subject="Wikipedia Pipeline Consumer WARNING"):
    """
    Send an email with SES
    """

    try:
        recipient = ses_recipient.replace(' ', '')
        recipient_list = recipient.split(',')
        response = ses_client.send_email(
            Destination={
                'ToAddresses': recipient_list,
            },
            Message={
                'Body': {
                    'Text': {
                        'Charset': "UTF-8",
                        'Data': body_text,
                    },
                },
                'Subject': {
                    'Charset': "UTF-8",
                    'Data': '[' + project_name + '] ' + subject,
                },
            },
            Source=ses_sender
        )
        logger.info('#: Mail sent to: %s' % recipient)
    except ClientError as e:
        logger.info("Failed to send email: %s" % e.response['Error']['Message'])


def do_big_query(event=None, context=None):
    """
    Do the big athena query on background and send the query id on a SQS queue
    """

    try: # SNS Message
        mess = event['Records'][0]['Sns']['Message'].strip("\n")
        message = json.loads(mess)
    except: # CRON Message
        message = event

    projects = message['projects']
    project_list = projects.split(',')

    # these are used into the SQS message of the next process.
    channel_name = message['channel_name'] # the name of the output channel
    channel_lang = message['channel_lang'] # the language of the output channel
    wiki_base_url = message['wiki_base_url']
    query_type = message['query_type'] # daily || hourly || hourly_yesterday

    try:
        # Do the add partition for the daily
        if query_type == 'daily':
            # Wikipedia daily is updated to yesterday: add all partitions required in case some old days are missing.
            first_date = datetime.strftime(datetime.now() - timedelta(366), '%Y-%m-%d') if 'first_date' not in message else message['first_date']
            last_date = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d') if 'last_date' not in message else message['last_date']

            q_partitions = "ALTER TABLE "+const_athena_db+".daily_csv ADD IF NOT EXISTS "
            sdate = datetime.now() - timedelta(1) # yesterday
            for i in range(366):
                day = sdate - timedelta(days=i)
                dp = datetime.strftime(day, '%Y%m%d')
                q_partitions += " PARTITION (date_p="+ dp +") "
        else:
            # Wikipedia hourly is updated to today. Just add each hour of today to the add partition
            # instead of checking if already added
            if query_type == 'hourly_yesterday':
                today_date = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')
            else:
                today_date = datetime.strftime(datetime.now(), '%Y-%m-%d')

            dp = today_date.replace('-', '')

            q_partitions = "ALTER TABLE "+const_athena_db+".hourly_csv ADD IF NOT EXISTS "

            for i in range(24):
                dh = str(i).zfill(2)
                q_partitions += " PARTITION (date_p="+ dp +",hour_p='" + dh + "') "

        resp_query1_id = query_athena(q_partitions, 'add partion')
        time.sleep(3)

        i = 0
        while i < 30:
            try:
                i = i + 1

                # get query execution
                query_status = athena_client.get_query_execution(QueryExecutionId=resp_query1_id)
                query_execution_status = query_status['QueryExecution']['Status']['State']

                logger.info("Add Partition "+ query_type +" Status:" + query_execution_status)

                if query_execution_status == 'SUCCEEDED':
                    break
                elif query_execution_status == 'FAILED' or query_execution_status == 'CANCELLED':
                    send_mail("Query Add Partition execution for id %s ended with status %s" % (resp_query1_id, query_execution_status))
                    break
                else:
                    raise Exception('Query status is RUNNING')

            except Exception:
                time.sleep(3)

        if query_type == 'daily':
            query = get_athena_big_query_daily(first_date, last_date, project_list, channel_name, channel_lang)
        elif query_type == 'hourly':
            query = get_athena_big_query_hourly(project_list, channel_name, channel_lang, "")
        else:
            query = get_athena_big_query_hourly(project_list, channel_name, channel_lang, "- interval '1' day")

        resp_query_id = query_athena(query, 'big query')
        logger.info("Response query hourly id: %s" % resp_query_id)

        queue = sqs_resource.get_queue_by_name(QueueName=queue_name_athena_create)

        sqs_message = {
            "query_execution_id": resp_query_id['QueryExecutionId'],
            "query_type": query_type,
            "channel_name": channel_name,
            "channel_lang": channel_lang,
            "wiki_base_url": wiki_base_url
        }

        queue.send_message(MessageBody=json.dumps(sqs_message))

    except Exception as e:
        logger.error(e)


def query_athena(athena_query, qtype = ''):
    """ Execute a query to Athena

    :type athena_query: string
    :param athena_query: The query Athena to be executed
    """
    try:
        resp_athena = athena_client.start_query_execution(
            QueryString=athena_query,
            QueryExecutionContext={
                'Database': const_athena_db
            },
            ResultConfiguration={
                'OutputLocation': const_athena_s3_output
            },
            WorkGroup=athena_workgroup_name # Default to primary
        )
    except ClientError as e:
        logger.error(e)
        raise e
    else:
        logger.info('#: Quering %s.....' % qtype)
        return resp_athena


def get_athena_big_query_daily(first_date, last_date, project_list, channel_name, channel_lang):
    """ Get the Athena query string, used to retrieve all data.

    :type first_date: string
    :param first_date: The first date (ie. 2018-09-20) to use inside the Athena query.

    :type last_date: string
    :param last_date: The last date (ie. 2019-09-20) to use inside the Athena query.

    :type proj: list
    :param proj: A list of projects (ie. ['it.z', 'it.z.m'])
    """

    proj_str = ''
    for p in project_list:
        proj_str = proj_str + "'" + p + "',"

    formatter = {
        "athena_db": const_athena_db,
        "athena_table_editorial_plan": "editorial_plans",
        "channel": channel_name,
        "lang": channel_lang,
        "iso_date_last": last_date,
        "iso_date_first": first_date,
        "int_date_last": int( last_date.replace('-', '') ),
        "int_date_first": int( first_date.replace('-', '') ),
        "projects": proj_str[:-1] # remove last comma
    }

    return """
WITH

    wiki_days_list AS (
        SELECT
            CAST( REPLACE( CAST( CAST( day_int AS DATE ) AS VARCHAR ), '-', '' ) AS INTEGER ) AS day_int
        FROM (
            VALUES ( SEQUENCE ( FROM_ISO8601_DATE('{iso_date_first}'), FROM_ISO8601_DATE('{iso_date_last}'), INTERVAL '1' DAY ) )
        ) AS t1( date_array )
        CROSS JOIN
            UNNEST( date_array ) AS t2( day_int )
    ),

    wiki_editorial_plan_data AS (
        SELECT page, title, category FROM {athena_db}.{athena_table_editorial_plan}
        WHERE channel='{channel}' AND lang='{lang}'
    ),

    wiki_editorial_plan_days AS (
        SELECT * FROM wiki_editorial_plan_data, wiki_days_list
    ),

    all_nonzero_data AS (
        SELECT ep.category, d.tot, d.date_p
        FROM {athena_db}.daily_csv AS d
        INNER JOIN {athena_db}.{athena_table_editorial_plan} AS ep
            ON d.project = ep.project AND d.page = ep.page
        WHERE CAST(date_p AS integer) >= {int_date_first} AND CAST(date_p AS integer) <= {int_date_last}
            AND ( d.project IN ( {projects} ) )
            AND ep.channel='{channel}' AND ep.lang='{lang}'
        ORDER BY category, date_p
    ),

    all_data AS (
        SELECT epd.category, epd.title, epd.page, COALESCE(tot, 0) AS tot, day_int
        FROM all_nonzero_data AS nzd
        RIGHT JOIN wiki_editorial_plan_days AS epd
            ON nzd.date_p = epd.day_int AND nzd.category = epd.category
        ORDER BY day_int
    ),

    all_projects_data AS (
        SELECT category, page, title, sum( tot ) AS global_tot, day_int
        FROM all_data
        GROUP BY category, page, title, day_int
        ORDER BY day_int
    )

-- Final --
SELECT category, page, title, array_agg( global_tot ) AS tot_array, array_agg( day_int ) AS date_array
FROM all_projects_data
GROUP BY category, page, title
ORDER BY category

""".format(**formatter)


def get_athena_big_query_hourly(project_list, channel_name, channel_lang, when=''):
    """ Get the Athena query string, used to retrieve all data.

    :type proj: list
    :param proj: A list of projects (ie. ['it.z', 'it.z.m'])

    Note that the view `view_merged_hours` must be manually created. See the readme for more.
    It contains the data of the last date.
    """

    proj_str = ''
    for p in project_list:
        # for consistency with the daily dump we use the same project list but
        # in the hourly dumps they are without the "z" so we need to remove it
        p_list = p.split('.')
        try:
            p_list.remove('z')
        except:
            pass
        p_hourly = '.'.join(p_list)

        proj_str = proj_str + "'" + p_hourly + "',"

    formatter = {
        "athena_db": const_athena_db,
        "athena_table_editorial_plan": 'editorial_plans',
        "channel": channel_name,
        "lang": channel_lang,
        "projects": proj_str[:-1], # remove last comma
        "when": when
    }

    return """
WITH

    last_hour_p_in_date_p AS (
        SELECT max(hour_p) as max_h, date_p
        FROM {athena_db}.hourly_csv
        GROUP BY date_p
    ),

    all_projects_data_hourly AS (
        SELECT ep.category, ep.page, ep.title,
            array_agg(
                ARRAY[
                    COALESCE(h00,0), COALESCE(h01,0), COALESCE(h02,0), COALESCE(h03,0),
                    COALESCE(h04,0), COALESCE(h05,0), COALESCE(h06,0), COALESCE(h07,0),
                    COALESCE(h08,0), COALESCE(h09,0), COALESCE(h10,0), COALESCE(h11,0),
                    COALESCE(h12,0), COALESCE(h13,0), COALESCE(h14,0), COALESCE(h15,0),
                    COALESCE(h16,0), COALESCE(h17,0), COALESCE(h18,0), COALESCE(h19,0),
                    COALESCE(h20,0), COALESCE(h21,0), COALESCE(h22,0), COALESCE(h23,0)
                ]
            )[1] as tot_array,
            array_agg( CAST( vmh.date_p AS integer ) )[1] AS date_array,
            array_agg( lhd.max_h )[1] as max_h_arr
        FROM {athena_db}.view_merged_hours AS vmh
            RIGHT OUTER JOIN {athena_db}.{athena_table_editorial_plan} AS ep ON vmh.page = ep.page
            JOIN last_hour_p_in_date_p AS lhd ON vmh.date_p = lhd.date_p
        WHERE vmh.project IN ( {projects} )
            and vmh.date_p = cast(date_format(( now() {when}), '%Y%m%d') AS integer)
            AND ep.channel='{channel}' AND ep.lang='{lang}'
        GROUP BY ep.category, ep.page, ep.title, vmh.date_p
        ORDER BY ep.category
    )

SELECT category, page, title,
        array_agg( tot_array ) as tot_array,
        array_agg( date_array ) as date_array,
        array_agg( max_h_arr ) as max_h_array
FROM all_projects_data_hourly
GROUP BY category, page, title
ORDER BY category
""".format(**formatter)
