#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, codecs, gzip, logging, re

def _compress_utf8_file(fullpath, delete_original = True):
    """
    Compress a UTF-8 encoded file using GZIP compression named *.gz. If `delete_original` is `True` [default: True],
    the original file specified by `delete_original` is removed after compression.
    """
    with codecs.open(fullpath, 'r', 'utf-8') as fin:
        with gzip.open(fullpath + '.gz', 'wb') as fout:
            for line in fin:
                fout.write(line.encode('utf-8'))

    if delete_original:
        os.remove(fullpath)

def _get_env(envar):
    return os.environ[envar] if envar in os.environ else 'ERR_'+envar

def _get_logger():
    logging.basicConfig(level=logging.DEBUG, format='%(message)s')
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logging.getLogger('boto3').setLevel(logging.ERROR)
    logging.getLogger('botocore').setLevel(logging.ERROR)
    return logger


def _natural_key(in_str):
    """
    Sort by natural keys
    """
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', in_str)]